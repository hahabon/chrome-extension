
var texts = ['存款', '取款', '充值', '提现','提款','支付','转账','扫码','银行卡','钱包'];
var visibility = false;
var getTextNodesIn = function (elem, opt_fnFilter) {
    var textNodes = [];
    if (elem) {
        for (var nodes = elem.childNodes, i = nodes.length; i--;) {
            var node = nodes[i],
                nodeType = node.nodeType;
            if (nodeType == 3) {
                if (!opt_fnFilter || opt_fnFilter(node, elem)) {
                    textNodes.push(node);
                }
            } else if (nodeType == 1 || nodeType == 9 || nodeType == 11) {
                textNodes = textNodes.concat(getTextNodesIn(node, opt_fnFilter));
            }
        }
    }
    return textNodes;
}
var setDomVisible = function (visibility) {
    if (!visibility) {
        visibility = false;
    }    
    getTextNodesIn(document.body, function (textNode, parent) {
        let text = textNode.nodeValue.trim();
        text = text.replaceAll(".",'x');// 避免 ...
        //console.log(textNode)
        if(!text)
        {
            
        }else
        {
            
            for(let compareText of texts)
            {        
                if(parent.tagName == 'BODY')
                {
                    break;
                }
                
                if(text.match(compareText) != null)
                {                
                    console.log("compareText",compareText,"text",text)
                    if(visibility == false)
                    {
                        parent.style.setProperty('visibility', 'hidden', 'important');
                    }else
                    {
                        parent.style.setProperty('visibility', 'visible', 'important');
                    }
                    
                    break;
                }
            }
        }
        
        

    })    
}
// 偵測是否有body change
function detectDomChange()
{
    console.log("arnold detectDomChange begin");
    var target = document.querySelector('body');

    // create an observer instance
    var observer = new MutationObserver(function(mutations) {
        console.log("detectDomChange ",visibility);   
        setTimeout(() => setDomVisible(visibility),500)
        
        
    });

    // configuration of the observer:
    var config = { attributes: true, childList: true, characterData: true };

    // pass in the target node, as well as the observer options
    observer.observe(target, config);

    
}
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.command === 'hide') {
        visibility = false;
        setDomVisible(false);
        console.log("sync command : hide");
    } else {
        visibility = true;
        setDomVisible(true);
        console.log("sync command : show");
    }
    sendResponse({
        result: "success"
    });
});
window.onload = function () {
    chrome.storage.sync.get('hide', function (data) {
        if (data.hide) {
            visibility = false;
            setDomVisible(false);
            console.log("default : hide");
        } else {
            visibility = true;
            setDomVisible(true);
            console.log("default : show");
        }
        detectDomChange();
    });
}